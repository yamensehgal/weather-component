import React from 'react';
import ReactDOM from 'react-dom';

var Header = React.createClass({
      render: function() {
        return (
     <header> 
            <nav className="navbar navbar-default header-custom">
              <div className="container-fluid ">
                <div className="navbar-header">
                  <a className="logo-custom" href="#">{this.props.headerName}</a>
                </div>
              </div>
            </nav> 
      </header>
        );
      }
    });

	   module.exports = Header;