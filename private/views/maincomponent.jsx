import React from 'react';
import ReactDOM from 'react-dom';
import Header from './header.jsx';
import SearchBox from './searchbox.jsx';
import CityDetails from './citydetails.jsx';
import WeatherDetails from './weatherdetails.jsx';
import {API_URL,IMAGE_URL} from '../constants/constants.jsx';

  var MainComponent = React.createClass({
  getInitialState() {
    return { data: [], city:'gurgaon' };
  },
   updateCity: function(cityName) {
          var updatedCity = cityName;
           this.setState({city: updatedCity});
           this.fetchData(updatedCity);
        }, 
   
   fetchData : function(city){

    var URL = API_URL + city; 
  
    return fetch(URL)
      .then((response) => response.json())
      .then((responseJson) => {
      this.setState({data: responseJson});
        })
      .catch((error) => {
        console.error(error);
      });
    },
     componentDidMount() {
      this.fetchData(this.state.city);
  },
	   render: function() {
        var data = this.state.data;

         if (Object.keys(data).length ==  0) {
                  return null;
        }
		    return (
		     <div className="container"><Header headerName = "5 Day Weather Forecast"/><SearchBox updateCity = {this.updateCity}/><CityDetails cityDetails = {this.state.data.city} /> <WeatherDetails weatherDetails={this.state.data.list} /> </div>
		    );
		  }
    });
		   module.exports = MainComponent;
