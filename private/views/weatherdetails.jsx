import React from 'react';
import ReactDOM from 'react-dom';
import {API_URL,IMAGE_URL} from '../constants/constants.jsx';

 var WeatherDetails = React.createClass({
      render: function() {
      
        return (
     <div className="weatherDetails">
     {this.props.weatherDetails.map(function(list,index){
          return (
          
          <div className="panel panel-default" style={{display: 'inline-block', padding: '10px'}} key={index}>Date: {list.dt_txt} <br/> Temperature : {list.main.temp}<br/> Min : {list.main.temp_min} <br/> Max : {list.main.temp_max}<br/> Humidity : {list.main.humidity} %

            {list.weather.map(function(item,index){
                var iconurl = IMAGE_URL + item.icon + ".png";
              return (
              <div key={index}> Sky : <img src={iconurl} title = {item.main}/> </div>       
              )
            })}

          Cloudiness: {list.clouds.all} % <br/> Wind : {list.wind.speed} meter/sec  </div>

          );  


     })} 

     </div>
        );
      }
    });
	   module.exports = WeatherDetails;