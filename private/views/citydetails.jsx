import React from 'react';
import ReactDOM from 'react-dom';
   var CityDetails = React.createClass({
    
      render: function() {

        return (
     <div className="well"><label className="detailLabel">City </label> : <label  className="label label-primary" >{this.props.cityDetails.name}</label> <label className="detailLabel">Country </label> : <label  className="label label-primary"> {this.props.cityDetails.country}</label><label className="detailLabel">Latitude </label>: <label  className="label label-primary">{this.props.cityDetails.coord.lat}</label><label className="detailLabel">Longitude</label> : <label  className="label label-primary" > {this.props.cityDetails.coord.lon}</label></div>
        );
      }
    });
	   module.exports = CityDetails;