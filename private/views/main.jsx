import React from 'react';
import ReactDOM from 'react-dom';
import MainComponent from './maincomponent.jsx';

ReactDOM.render(
   <MainComponent />,
		    document.getElementById('projectcontent')
		  );