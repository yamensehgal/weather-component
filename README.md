# 5 day Weather Forcast Component:

As the name suggests, 5 day Weather Forecast Component allows us display 5 day weather forecast for a location of your choice.


## Features:

1. Component allows us to display 5 day weather forecast of any location.
2. It gives information about current temperature, min temperature, max temperature, humidity, Cloudiness, and wind speed information.
3. Also, User can enter any city for which he/she wants to know 5 day weather forecast.

## Usage/ Installation:

1. Unzip the file weather-component.zip .
2. Open weather-component.zip
3. Make sure that node is installed.
4. Open weather-component and then run npm install.
5. run  npm start.
6. open http://localhost:8081 in browser.  


## Technologies Used:

1. React.js Library
2. Bootstrap.js Library
4. Bootstrap CSS
5. Javascript
6. HTML
7. CSS
8. Mocha, Chai and Enzyme
9. Webpack

Page is divided into different react components which are :

Page: Index.html

1. <MainComponent /> : This is the main component which is exported. It includes all other components. Also, ajax hit to openweathermap is made in componentWillMount function of this component..

2. <Header />: Header component is used for header of the page. This is a reusable component in which Name of the page is passed as props.

3. <CityDetails />:  The second major component of the page is CityDetails. This component is used to display name of the city, name of the country, its latitude and its longitude.

4. <WeatherDetails />:This component is used to day 5 day weather forecast of a particular location.

5. <SearchBox /> : Search box component allows user to type and submit a particular city for which he/she wants to know 5 day weather forecast.

## Testing of React Components:

For testing React components, JavaScript Testing utility enzyme is used.

Whenever npm start happens, test scripts automatically gets executed.