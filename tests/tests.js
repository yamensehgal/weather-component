import React from 'react';
import { expect, assert } from 'chai';
import { shallow, mount, render } from 'enzyme';
import MainComponent  from '../private/views/maincomponent';
import Header  from '../private/views/header';
import SearchBox  from '../private/views/searchbox';
import CityDetails  from '../private/views/citydetails';
import WeatherDetails  from '../private/views/weatherdetails';
import {spy} from 'sinon';


  describe('Test cases for Main Component(weather component) : ', () => {

  it('MainComponent should exist', () => {
    let wrapper = shallow(<MainComponent />)
    expect(wrapper).to.exist;
  });


  it('should show the <Header />, <SearchBox />, <CityDetails />, <WeatherDetails /> when data is fetched', () => {
        const wrapper = shallow(<MainComponent />);
        wrapper.setState({ data: {"city":{"id":1270642,"name":"Gurgaon","coord":{"lon":77.033333,"lat":28.466667},"country":"IN","population":0,"sys":{"population":0}},"cod":"200","message":0.0143,"cnt":1,"list":[{"dt":1484762400,"main":{"temp":10.3,"temp_min":7.65,"temp_max":10.3,"pressure":1011.1,"sea_level":1037.31,"grnd_level":1011.1,"humidity":88,"temp_kf":2.65},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"02n"}],"clouds":{"all":8},"wind":{"speed":1.4,"deg":309},"rain":{},"sys":{"pod":"n"},"dt_txt":"2017-01-18 18:00:00"}]}, city:'gurgaon' });
        const Header = wrapper.find('Header');
        const SearchBox = wrapper.find('SearchBox');
        const WeatherDetails = wrapper.find('WeatherDetails');
        const CityDetails = wrapper.find('CityDetails');

        assert.equal(Header.length, 1);
        assert.equal(SearchBox.length, 1);
        assert.equal(WeatherDetails.length, 1);
        assert.equal(CityDetails.length, 1);

        });

    it('title in the header should be same as passed as props', () => {
          const HEADER_PROPS = {
        headerName: '5 Day Weather Forecast'
        };
    const wrapper = shallow(<Header {...HEADER_PROPS} />);
    expect(wrapper.text()).to.equal('5 Day Weather Forecast');

        });

    it('search box should have default value as gurgaon and submit button should call update method', () => {
         const SearchProps = {
                updateCity: spy()

    };
    
    const wrapper = mount(<SearchBox {...SearchProps}/>);
      const input = wrapper.find('input').props().value;
      expect(wrapper.ref('cityName').get(0).value).to.equal("gurgaon");
          const submitButton = wrapper.find('button');
          submitButton.simulate('click');
          assert.ok(SearchProps.updateCity.calledOnce);

        });

    it('Weather details component should display details of weather', () => {
         const weatherPropDetails = {"list":[{"dt":1484762400,"main":{"temp":10.3,"temp_min":7.65,"temp_max":10.3,"pressure":1011.1,"sea_level":1037.31,"grnd_level":1011.1,"humidity":88,"temp_kf":2.65},"weather":[{"id":800,"main":"Clear","description":"clear sky","icon":"02n"}],"clouds":{"all":8},"wind":{"speed":1.4,"deg":309},"rain":{},"sys":{"pod":"n"},"dt_txt":"2017-01-18 18:00:00"}]};
         const WeatherProps = {
                weatherDetails: weatherPropDetails.list

    };
    
     const wrapper = mount(<WeatherDetails {...WeatherProps}/>);
              const weatherDiv = wrapper.childAt(0);
              expect(weatherDiv.type()).to.equal('div');
            expect(wrapper.find('.panel')).to.have.length(1);
    });


     it('City details component should show name of city, Country, its Latitude and its Longitute ', () => {
           const CITY_PROPS = {};
           CITY_PROPS.city = {};
           CITY_PROPS.id = "1270642";
           CITY_PROPS.name = "Gurgaon";
           CITY_PROPS.country = "IN"
           CITY_PROPS.coord = {};
           CITY_PROPS.coord.lon = 77.033333;
           CITY_PROPS.coord.lat  = 28.466667;
          const CITY_DETAILS = {
            cityDetails: CITY_PROPS
        };
          const wrapper = shallow(<CityDetails {...CITY_DETAILS}/>);      
          const cityName = wrapper.childAt(2);
          const countryName = wrapper.childAt(5);
      const latituteName = wrapper.childAt(8);
      const longName = wrapper.childAt(10);

      expect(cityName.type()).to.equal('label');
      expect(countryName.type()).to.equal('label');
      expect(latituteName.type()).to.equal('label');
      expect(longName.type()).to.equal('label');
     
      expect(cityName.text()).to.equal('Gurgaon');
      expect(countryName.text()).to.equal(' IN');
      expect(latituteName.text()).to.equal('28.466667');
      expect(longName.text()).to.equal(' 77.033333');

        });

});

